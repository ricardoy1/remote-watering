# README #

SignalR experiment to fire a "water" signal into a listener that can activate a watering system.

The final goal is to elaborate a system that allows users to water their plants from a distant location, like in: http://rsyoussef.com/overseas-watering/