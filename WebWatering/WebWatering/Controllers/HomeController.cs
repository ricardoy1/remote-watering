﻿using System.Web.Mvc;

namespace WebWatering.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Water()
        {
            return View("Index");
        }
    }
}
