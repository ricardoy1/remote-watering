﻿using Microsoft.AspNet.SignalR;

namespace WebWatering.Hubs
{
    using System.Threading.Tasks;

    public class WateringHub : Hub
    {
        public void Water(string sign)
        {
            Clients.All.water(sign);
        }

        public void WateringDone(string result)
        {
            Clients.All.wateringDone(result);
        }

        public void WateringStarted()
        {
            Clients.All.wateringStarted();
        }
    }
}